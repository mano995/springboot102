package com.example.demo;

import java.util.List;

public interface CustomerRepo {

    List<Customer> getCustomer();
}
