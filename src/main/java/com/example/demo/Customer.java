package com.example.demo;

public class Customer {
     private final Long id;
     private final String name;

     public Customer(Long id, String name) {
        this.id = id;
        this.name = name;
    }
}